import {BaseCookie} from "../../waffle_dough/BaseCookie";
import {BaseChocolate} from "./BaseChocolate";

export class MilkyChocolate extends BaseChocolate {

    constructor(waffleToDecorate: BaseCookie) {
        super(waffleToDecorate);
        this.price = 2.0;
    }

    public getDescription(): string {

        return this.baseCookie.getDescription() + " , Milky Chocolate ";
    }
}
