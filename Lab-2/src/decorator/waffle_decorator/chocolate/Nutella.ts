import {BaseCookie} from "../../waffle_dough/BaseCookie";
import {BaseChocolate} from "./BaseChocolate";

export class Nutella extends BaseChocolate {

    constructor(waffleToDecorate: BaseCookie) {
        super(waffleToDecorate);
        this.price = 2.0;
    }

    public getDescription(): string {
        return this.baseCookie.getDescription() + " , Nutella ";
    }

}
