import {BaseCookie} from "../../waffle_dough/BaseCookie";
import {BaseNuts} from "./BaseNuts";

export class Hazelnut extends BaseNuts {

    constructor(waffleToDecorate: BaseCookie) {
        super(waffleToDecorate);
        this.price = 1.00;
    }

    public getDescription(): string {
        return this.baseCookie.getDescription() + " , Hazelnut ";
    }
}
