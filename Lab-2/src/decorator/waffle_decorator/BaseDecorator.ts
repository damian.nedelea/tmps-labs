import {BaseCookie} from "../waffle_dough/BaseCookie";

export abstract class BaseDecorator extends BaseCookie {

    public getDescription(): string {
        return super.getDescription();
    }
}
