import {BaseDecorator} from "../BaseDecorator";
import {BaseCookie} from "../../waffle_dough/BaseCookie";

export abstract class BaseSauce extends BaseDecorator {

    public baseCookie: BaseCookie;

    constructor(waffleToDecorate: BaseCookie) {
        super()
        this.baseCookie = waffleToDecorate;
    }


    public getDescription(): string {
        return super.getDescription();
    }


    public getPrice(): number {
        return (this.baseCookie.getPrice() + this.price);
    }
}
