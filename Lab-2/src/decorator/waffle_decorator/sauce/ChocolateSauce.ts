import {BaseCookie} from "../../waffle_dough/BaseCookie";
import {BaseSauce} from "./BaseSauce";

export class ChocolateSauce extends BaseSauce {

    constructor(waffleToDecorate: BaseCookie) {
        super(waffleToDecorate);
        this.price = 1.00;
    }

    public getDescription(): string {
        return this.baseCookie.getDescription() + " , Chocolate Sauce ";
    }
}
