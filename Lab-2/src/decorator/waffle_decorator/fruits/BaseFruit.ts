import {BaseDecorator} from "../BaseDecorator";
import {BaseCookie} from "../../waffle_dough/BaseCookie";

export abstract class BaseFruit extends BaseDecorator {

    public baseCookie: BaseCookie;

    constructor(waffleToDecorate: BaseCookie){
        super();
        this.baseCookie = waffleToDecorate;
    }


    public getPrice(): number {
        return (this.baseCookie.getPrice() + this.price);
    }
}
