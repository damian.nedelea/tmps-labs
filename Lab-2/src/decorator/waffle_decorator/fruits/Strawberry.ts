import {BaseCookie} from "../../waffle_dough/BaseCookie";
import {BaseFruit} from "./BaseFruit";

export class Strawberry extends BaseFruit {

    constructor(waffleToDecorate: BaseCookie) {
        super(waffleToDecorate);
        this.price = 0.5;
    }

    public getDescription(): string {
        return this.baseCookie.getDescription() + " , Strawberry ";
    }

}
