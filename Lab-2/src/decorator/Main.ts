import {FreeGlutenCookie} from "./waffle_dough/FreeGlutenCookie";
import {Coconut} from "./waffle_decorator/nuts/Coconut";
import {BelgianCookie} from "./waffle_dough/BelgianCookie";
import {CaramelSauce} from "./waffle_decorator/sauce/CaramelSauce";
import {Banana} from "./waffle_decorator/fruits/Banana";
import {RaspberrySauce} from "./waffle_decorator/sauce/RaspberrySauce";
import {Kiwi} from "./waffle_decorator/fruits/Kiwi";
import {Almond} from "./waffle_decorator/nuts/Almond";
import {WhiteChocolate} from "./waffle_decorator/chocolate/WhiteChocolate";
import {Walnut} from "./waffle_decorator/nuts/Walnut";
import {Nutella} from "./waffle_decorator/chocolate/Nutella";
import {Strawberry} from "./waffle_decorator/fruits/Strawberry";
import {MilkyChocolate} from "./waffle_decorator/chocolate/MilkyChocolate";
import {WrapCookie} from "./waffle_dough/WrapCookie";
import {DarkChocolate} from "./waffle_decorator/chocolate/DarkChocolate";
import {Pineapple} from "./waffle_decorator/fruits/Pineapple";
import {ChocolateSauce} from "./waffle_decorator/sauce/ChocolateSauce";

let waffle1 = new BelgianCookie();
console.log(waffle1.getDescription() + " $" + waffle1.getPrice());

// Now we add some condiment
waffle1 = new Nutella(waffle1);
console.log(waffle1.getDescription() + " $ " + waffle1.getPrice());

waffle1 = new ChocolateSauce(waffle1);
console.log(waffle1.getDescription() + " $ " + waffle1.getPrice());

waffle1 = new Kiwi(waffle1);
console.log(waffle1.getDescription() + " $ " + waffle1.getPrice());


// Create second waffle

let waffle2 = new WrapCookie();
console.log(waffle2.getDescription() + " $ " + waffle2.getPrice());

waffle2 = new WhiteChocolate(waffle2);
console.log(waffle2.getDescription() + " $ " + waffle2.getPrice());

waffle2 = new CaramelSauce(waffle2);
console.log(waffle2.getDescription() + " $ " + waffle2.getPrice());

waffle2 = new Strawberry(waffle2);
console.log(waffle2.getDescription() + " $ " + waffle2.getPrice());

waffle2 = new Banana(waffle2);
console.log(waffle2.getDescription() + " $ " + waffle2.getPrice());

waffle2 = new Almond(waffle2);
console.log(waffle2.getDescription() + " $ " + waffle2.getPrice());

// create third waffle

let waffle3 = new FreeGlutenCookie();
console.log(waffle3.getDescription() + " $ " + waffle3.getPrice());

waffle3 = new DarkChocolate(waffle3);
console.log(waffle3.getDescription() + " $ " + waffle3.getPrice());

waffle3 = new MilkyChocolate(waffle3);
console.log(waffle3.getDescription() + " $ " + waffle3.getPrice());

waffle3 = new RaspberrySauce(waffle3);
console.log(waffle3.getDescription() + " $ " + waffle3.getPrice());

waffle3 = new Strawberry(waffle3);
console.log(waffle3.getDescription() + " $ " + waffle3.getPrice());

waffle3 = new Pineapple(waffle3);
console.log(waffle3.getDescription() + " $ " + waffle3.getPrice());

waffle3 = new Kiwi(waffle3);
console.log(waffle3.getDescription() + " $ " + waffle3.getPrice());

waffle3 = new Coconut(waffle3);
console.log(waffle3.getDescription() + " $ " + waffle3.getPrice());

waffle3 = new Walnut(waffle3);
console.log(waffle3.getDescription() + " $ " + waffle3.getPrice());
