export abstract class BaseCookie {
    public description: string = "";
    public price: number;


    public getDescription(): string {

        return this.description;
    }

    public getPrice(): number {
        return this.price;
    }
}
