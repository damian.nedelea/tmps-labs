import {BaseCookie} from "./BaseCookie";

export class FreeGlutenCookie extends BaseCookie {

    constructor() {
        super()
        this.description = "Free gluten waffle dough";
        this.price = 17.0;
    }
}
