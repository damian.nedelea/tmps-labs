import {BaseCookie} from "./BaseCookie";

export class WheatCookie extends BaseCookie {

    constructor() {
        super()
        this.description = "waffle dough with wheat flour";
        this.price = 15.0;
    }
}
