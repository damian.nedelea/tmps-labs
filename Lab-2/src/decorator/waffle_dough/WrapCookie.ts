import {BaseCookie} from "./BaseCookie";

export class WrapCookie extends BaseCookie {

    constructor() {
        super()
       this.description = "Wrap waffle dough";
       this.price = 18.0;
   }
}
