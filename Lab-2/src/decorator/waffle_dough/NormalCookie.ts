import {BaseCookie} from "./BaseCookie";

export class NormalCookie extends BaseCookie {

    constructor(){
        super()
        this.description = " default waffle dough ";
        this.price = 13.0;
    }
}
