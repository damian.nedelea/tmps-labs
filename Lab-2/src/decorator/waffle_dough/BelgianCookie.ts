import {BaseCookie} from "./BaseCookie";

export class BelgianCookie extends BaseCookie {
    constructor() {
        super()
        this.description = "Belgian waffle dough";
        this.price = 16.0;
    }
}
