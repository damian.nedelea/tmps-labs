interface FoodService {
    makeChicken (salt: string): void;
    makeNoodle (salt: string): void
}

class Food {
    public type: string
    public salt: string

    public constructor (type: string, salt: string) {
        this.type = type
        this.salt = salt
        this.cook()
    }

    public cook (): void {
        console.log(`Type: ${this.type}，Weight: ${this.salt}`)
    }
}

class FoodServiceReal implements FoodService {
    public chicken: Food
    public Noodle: Food

    public makeChicken (salt: string): any {
        this.chicken = new Food('chicken', salt)
    }
    public makeNoodle (salt: string): any {
        this.Noodle = new Food('noodle', salt)
    }
}

class FoodServiceProxy implements FoodService {
    private FoodServiceReal: FoodServiceReal

    private prepareFood () {
        if (!this.FoodServiceReal) {
            this.FoodServiceReal = new FoodServiceReal()
        }
    }

    public makeChicken () {
        console.log('Start making chicken right now')
        console.log('==========')
        this.prepareFood()
        this.FoodServiceReal.makeChicken('500g')
        console.log('==========')
        console.log('Chicken is ready')
    }

    public makeNoodle () {
        console.log('Start making noodles now')
        console.log('==========')
        this.prepareFood()
        this.FoodServiceReal.makeNoodle('100g')
        console.log('==========')
        console.log('Noodles are ready')
    }
}

const food = new FoodServiceProxy()
food.makeChicken()
console.log('========')
food.makeNoodle()
