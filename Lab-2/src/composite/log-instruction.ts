import {SingleInstruction} from "./single-instruction";

export class LogInstruction extends SingleInstruction {
    log: string;

    constructor(name: string, log: string) {
        super(name);

        this.log = log;
    }

    execute() {
        console.log(`${this.name}: ${this.log}`);
        return true;
    }
}
