import {CompositeInstructionSet} from "./composite-instruction-set";
import {LogInstruction} from "./log-instruction";
import {TaskRunner} from "./task-runner";

const startUpLogInstruction = new LogInstruction('Starting', 'Task runner booting up...');
const compositeInstruction = new CompositeInstructionSet('Composite');

// Now let's define some sub tasks for the CompositeInstructionSet
const firstSubTask = new LogInstruction('Composite 1', 'The first sub task');
const secondSubTask = new LogInstruction('Composite 2', 'The second sub task');

// Let's add these sub tasks as children to the CompositeInstructionSet we created earlier
compositeInstruction.addChild(firstSubTask);
compositeInstruction.addChild(secondSubTask);

// Now let's create our TaskRunner with our Tasks
const taskRunner = new TaskRunner([startUpLogInstruction, compositeInstruction]);

// Finally, we'll ask the TaskRunner to run the tasks
taskRunner.runTasks();
