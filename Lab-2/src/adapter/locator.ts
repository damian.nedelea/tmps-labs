interface ILocator {
    geocode(): string;
}

export class Locator implements ILocator {
    private readonly latitude: number;
    private readonly longitude: number;

    constructor(latitude: number, longitude: number) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public geocode(): string {
        return `The specified location (${this.latitude}, ${this.longitude}) returns London.`;
    }
}
