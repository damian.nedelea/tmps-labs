export interface IGeoCoder {
    locate(): string;
}

export class GeoCoder implements IGeoCoder {
    private readonly latitude: number;
    private readonly longitude: number;
    constructor(latitude: number, longitude: number) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public locate(): string {
        return `${this.latitude} | ${this.longitude} is in London.`
    }
}
