import {GeoCoder, IGeoCoder} from "./geocoder";
import {Locator} from "./locator";

type LocationTypes = 'GeoCoder' | 'Locator'

export class GeocodingAdapter implements IGeoCoder {
    private readonly latitude: number;
    private readonly longitude: number;
    private readonly type: LocationTypes;
    constructor(latitude: number, longitude: number, type: LocationTypes) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.type = type;
    }

    locate(): string {
        if (this.type === 'GeoCoder') {
            const geocoder = new GeoCoder(this.latitude, this.longitude);
            return geocoder.locate();

        } else if (this.type === 'Locator') {
            const locator = new Locator(this.latitude, this.longitude);
            return locator.geocode();

        } else {
            throw new Error('Please use either GeoCoder or Locator');
        }
    }
}
