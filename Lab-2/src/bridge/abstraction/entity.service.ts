import {IApiService} from "../models/api.service.";

export abstract class EntityService {
    apiService: IApiService;

    constructor(apiService: IApiService) {
        this.apiService = apiService;
    }
}
