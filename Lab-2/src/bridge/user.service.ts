import {EntityService} from "./abstraction/entity.service";
import {IApiService} from "./models/api.service.";

export interface User {
    id: string;
    name: string;
    email: string;
}

export class UserService extends EntityService {
    activeUser: User;

    constructor(apiService: IApiService) {
        super(apiService);
    }

    addNewUser(user: User) {
        this.apiService.add(user);
    }

    // Here we perform some logic custom to the UserService
    // But use the underlying bridge between the concrete
    // implementation and the abstraction to update the
    // active user after some custom logic
    setActiveUserEmail(email: string) {
        this.activeUser.email = email;
        this.apiService.update(this.activeUser);
    }
}
