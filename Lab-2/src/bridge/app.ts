import {IApiService} from "./models/api.service.";
import {User, UserService} from "./user.service";

export class MongoDBService implements IApiService {
    add<T>(entity: T): void {
    }

    delete<T>(entity: T): void {
    }

    get<T>(): T {
        return {} as T;
    }

    getAll<T>(): T[] {
        return [];
    }

    update<T>(entity: T): void {
    }
    // etc
}

// Swap out the api service
const apiService = new MongoDBService();

// No need to change the rest!
const userService = new UserService(apiService);

userService.addNewUser({} as User);
