import {IApiService} from "./models/api.service.";

export class CustomApiService implements IApiService {
    get<T>(): T {
        return {} as T;
    }

    getAll<T>(): T[] {
        return [];
    }

    add<T>(entity: T): void {
        // etc
    }

    update<T>(entity: T): void {
        // etc
    }

    delete<T>(entity: T): void {
        // etc
    }
}
