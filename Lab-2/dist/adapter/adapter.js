"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GeocodingAdapter = void 0;
var geocoder_1 = require("./geocoder");
var locator_1 = require("./locator");
var GeocodingAdapter = /** @class */ (function () {
    function GeocodingAdapter(latitude, longitude, type) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.type = type;
    }
    GeocodingAdapter.prototype.locate = function () {
        if (this.type === 'GeoCoder') {
            var geocoder = new geocoder_1.GeoCoder(this.latitude, this.longitude);
            return geocoder.locate();
        }
        else if (this.type === 'Locator') {
            var locator = new locator_1.Locator(this.latitude, this.longitude);
            return locator.geocode();
        }
        else {
            throw new Error('Please use either GeoCoder or Locator');
        }
    };
    return GeocodingAdapter;
}());
exports.GeocodingAdapter = GeocodingAdapter;
