"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChocolateSauce = void 0;
var BaseSauce_1 = require("./BaseSauce");
var ChocolateSauce = /** @class */ (function (_super) {
    __extends(ChocolateSauce, _super);
    function ChocolateSauce(waffleToDecorate) {
        var _this = _super.call(this, waffleToDecorate) || this;
        _this.price = 1.00;
        return _this;
    }
    ChocolateSauce.prototype.getDescription = function () {
        return this.baseCookie.getDescription() + " , Chocolate Sauce ";
    };
    return ChocolateSauce;
}(BaseSauce_1.BaseSauce));
exports.ChocolateSauce = ChocolateSauce;
