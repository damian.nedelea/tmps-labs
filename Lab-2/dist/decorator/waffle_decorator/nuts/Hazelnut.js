"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.Hazelnut = void 0;
var BaseNuts_1 = require("./BaseNuts");
var Hazelnut = /** @class */ (function (_super) {
    __extends(Hazelnut, _super);
    function Hazelnut(waffleToDecorate) {
        var _this = _super.call(this, waffleToDecorate) || this;
        _this.price = 1.00;
        return _this;
    }
    Hazelnut.prototype.getDescription = function () {
        return this.baseCookie.getDescription() + " , Hazelnut ";
    };
    return Hazelnut;
}(BaseNuts_1.BaseNuts));
exports.Hazelnut = Hazelnut;
