"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.BelgianWaffle = void 0;
var BaseWaffle_1 = require("./BaseWaffle");
var BelgianWaffle = /** @class */ (function (_super) {
    __extends(BelgianWaffle, _super);
    function BelgianWaffle() {
        var _this = _super.call(this) || this;
        _this.description = "Belgian waffle dough";
        _this.price = 16.0;
        return _this;
    }
    return BelgianWaffle;
}(BaseWaffle_1.BaseWaffle));
exports.BelgianWaffle = BelgianWaffle;
