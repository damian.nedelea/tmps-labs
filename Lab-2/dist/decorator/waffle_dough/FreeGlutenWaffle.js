"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.FreeGlutenWaffle = void 0;
var BaseWaffle_1 = require("./BaseWaffle");
var FreeGlutenWaffle = /** @class */ (function (_super) {
    __extends(FreeGlutenWaffle, _super);
    function FreeGlutenWaffle() {
        var _this = _super.call(this) || this;
        _this.description = "Free gluten waffle dough";
        _this.price = 17.0;
        return _this;
    }
    return FreeGlutenWaffle;
}(BaseWaffle_1.BaseWaffle));
exports.FreeGlutenWaffle = FreeGlutenWaffle;
