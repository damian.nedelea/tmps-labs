"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.WrapWaffle = void 0;
var BaseWaffle_1 = require("./BaseWaffle");
var WrapWaffle = /** @class */ (function (_super) {
    __extends(WrapWaffle, _super);
    function WrapWaffle() {
        var _this = _super.call(this) || this;
        _this.description = "Wrap waffle dough";
        _this.price = 18.0;
        return _this;
    }
    return WrapWaffle;
}(BaseWaffle_1.BaseWaffle));
exports.WrapWaffle = WrapWaffle;
