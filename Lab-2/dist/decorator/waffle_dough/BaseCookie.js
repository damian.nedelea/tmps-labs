"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseCookie = void 0;
var BaseCookie = /** @class */ (function () {
    function BaseCookie() {
        this.description = "";
    }
    BaseCookie.prototype.getDescription = function () {
        return this.description;
    };
    BaseCookie.prototype.getPrice = function () {
        return this.price;
    };
    return BaseCookie;
}());
exports.BaseCookie = BaseCookie;
