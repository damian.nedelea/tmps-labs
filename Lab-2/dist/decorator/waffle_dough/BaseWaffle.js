"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseWaffle = void 0;
var BaseWaffle = /** @class */ (function () {
    function BaseWaffle() {
        this.description = "";
    }
    BaseWaffle.prototype.getDescription = function () {
        return this.description;
    };
    BaseWaffle.prototype.getPrice = function () {
        return this.price;
    };
    return BaseWaffle;
}());
exports.BaseWaffle = BaseWaffle;
