"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.BelgianCookie = void 0;
var BaseCookie_1 = require("./BaseCookie");
var BelgianCookie = /** @class */ (function (_super) {
    __extends(BelgianCookie, _super);
    function BelgianCookie() {
        var _this = _super.call(this) || this;
        _this.description = "Belgian waffle dough";
        _this.price = 16.0;
        return _this;
    }
    return BelgianCookie;
}(BaseCookie_1.BaseCookie));
exports.BelgianCookie = BelgianCookie;
