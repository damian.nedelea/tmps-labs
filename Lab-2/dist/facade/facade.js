"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Facade = void 0;
var subsystem1_1 = require("./subsystem1");
var subsystem2_1 = require("./subsystem2");
var Facade = /** @class */ (function () {
    function Facade(playerPrice) {
        this.contract = new subsystem1_1.Contract(new Date(new Date().setDate(new Date().getDate() + 10)));
        this.funds = new subsystem2_1.TeamFunds(200000);
        this.playerContract = new Date();
        this.playerPrice = playerPrice;
    }
    Facade.prototype.buyPlayer = function () {
        if (this.contract.checkActiveContract(this.playerContract)) {
            console.log('Player has active contract');
        }
        else {
            console.log('Player has no active contract');
            if (!this.funds.checkFunds(this.playerPrice)) {
                console.log('Player is too expensive to buy.');
            }
            else {
                console.log('Player can be bought.');
            }
        }
    };
    return Facade;
}());
exports.Facade = Facade;
