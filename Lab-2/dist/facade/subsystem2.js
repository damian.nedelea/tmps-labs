"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TeamFunds = void 0;
var TeamFunds = /** @class */ (function () {
    function TeamFunds(total) {
        this.totalFunds = total;
    }
    TeamFunds.prototype.checkFunds = function (transferFee) {
        if (transferFee < this.totalFunds) {
            return true;
        }
        else {
            return false;
        }
    };
    return TeamFunds;
}());
exports.TeamFunds = TeamFunds;
