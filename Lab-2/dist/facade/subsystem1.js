"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Contract = void 0;
var Contract = /** @class */ (function () {
    function Contract(terminationDate) {
        this.contractTerminationDate = terminationDate;
    }
    Contract.prototype.checkActiveContract = function (date) {
        if (date < this.contractTerminationDate) {
            return true;
        }
        else {
            return false;
        }
    };
    return Contract;
}());
exports.Contract = Contract;
