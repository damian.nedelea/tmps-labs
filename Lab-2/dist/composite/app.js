"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var composite_instruction_set_1 = require("./composite-instruction-set");
var log_instruction_1 = require("./log-instruction");
var task_runner_1 = require("./task-runner");
var startUpLogInstruction = new log_instruction_1.LogInstruction('Starting', 'Task runner booting up...');
var compositeInstruction = new composite_instruction_set_1.CompositeInstructionSet('Composite');
// Now let's define some sub tasks for the CompositeInstructionSet
var firstSubTask = new log_instruction_1.LogInstruction('Composite 1', 'The first sub task');
var secondSubTask = new log_instruction_1.LogInstruction('Composite 2', 'The second sub task');
// Let's add these sub tasks as children to the CompositeInstructionSet we created earlier
compositeInstruction.addChild(firstSubTask);
compositeInstruction.addChild(secondSubTask);
// Now let's create our TaskRunner with our Tasks
var taskRunner = new task_runner_1.TaskRunner([startUpLogInstruction, compositeInstruction]);
// Finally, we'll ask the TaskRunner to run the tasks
taskRunner.runTasks();
