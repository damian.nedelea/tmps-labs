"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompositeInstructionSet = void 0;
var CompositeInstructionSet = /** @class */ (function () {
    function CompositeInstructionSet(name) {
        // Our composite instruction should have children
        // that can be any implementation of Instruction
        this.children = [];
        this.name = name;
    }
    CompositeInstructionSet.prototype.execute = function () {
        var successful = false;
        // We'll iterate through our children calling their execute method
        // We don't need to know if our child is a Composite Instruction Set
        // or just a SingleInstruction
        for (var _i = 0, _a = this.children; _i < _a.length; _i++) {
            var child = _a[_i];
            successful = child.execute();
            // If any of the child tasks fail, lets fail this one
            if (!successful) {
                return false;
            }
        }
        return true;
    };
    // Our CompositeInstructionSet needs a public API to manage it's children
    CompositeInstructionSet.prototype.addChild = function (child) {
        this.children.push(child);
    };
    CompositeInstructionSet.prototype.removeChild = function (child) {
        this.children = this.children.filter(function (c) { return c.name !== child.name; });
    };
    return CompositeInstructionSet;
}());
exports.CompositeInstructionSet = CompositeInstructionSet;
