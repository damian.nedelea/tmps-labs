"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
var entity_service_1 = require("./abstraction/entity.service");
var UserService = /** @class */ (function (_super) {
    __extends(UserService, _super);
    function UserService(apiService) {
        return _super.call(this, apiService) || this;
    }
    UserService.prototype.addNewUser = function (user) {
        this.apiService.add(user);
    };
    // Here we perform some logic custom to the UserService
    // But use the underlying bridge between the concrete
    // implementation and the abstraction to update the
    // active user after some custom logic
    UserService.prototype.setActiveUserEmail = function (email) {
        this.activeUser.email = email;
        this.apiService.update(this.activeUser);
    };
    return UserService;
}(entity_service_1.EntityService));
exports.UserService = UserService;
