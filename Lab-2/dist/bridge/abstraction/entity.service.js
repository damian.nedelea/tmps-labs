"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntityService = void 0;
var EntityService = /** @class */ (function () {
    function EntityService(apiService) {
        this.apiService = apiService;
    }
    return EntityService;
}());
exports.EntityService = EntityService;
