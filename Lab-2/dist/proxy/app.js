"use strict";
var Food = /** @class */ (function () {
    function Food(type, salt) {
        this.type = type;
        this.salt = salt;
        this.cook();
    }
    Food.prototype.cook = function () {
        console.log("Type: " + this.type + "\uFF0CWeight: " + this.salt);
    };
    return Food;
}());
var FoodServiceReal = /** @class */ (function () {
    function FoodServiceReal() {
    }
    FoodServiceReal.prototype.makeChicken = function (salt) {
        this.chicken = new Food('chicken', salt);
    };
    FoodServiceReal.prototype.makeNoodle = function (salt) {
        this.Noodle = new Food('noodle', salt);
    };
    return FoodServiceReal;
}());
var FoodServiceProxy = /** @class */ (function () {
    function FoodServiceProxy() {
    }
    FoodServiceProxy.prototype.prepareFood = function () {
        if (!this.FoodServiceReal) {
            this.FoodServiceReal = new FoodServiceReal();
        }
    };
    FoodServiceProxy.prototype.makeChicken = function () {
        console.log('Start making chicken right now');
        console.log('==========');
        this.prepareFood();
        this.FoodServiceReal.makeChicken('500g');
        console.log('==========');
        console.log('Chicken is ready');
    };
    FoodServiceProxy.prototype.makeNoodle = function () {
        console.log('Start making noodles now');
        console.log('==========');
        this.prepareFood();
        this.FoodServiceReal.makeNoodle('100g');
        console.log('==========');
        console.log('Noodles are ready');
    };
    return FoodServiceProxy;
}());
var food = new FoodServiceProxy();
food.makeChicken();
console.log('========');
food.makeNoodle();
