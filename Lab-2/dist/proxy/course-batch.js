"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CourseBatch = void 0;
var CourseBatch = /** @class */ (function () {
    function CourseBatch() {
        this.listOfStudents = [];
    }
    CourseBatch.prototype.totalStudents = function () {
        return this.listOfStudents.length + 1;
    };
    CourseBatch.prototype.registerStudent = function (name) {
        this.listOfStudents.push(name);
        console.log(" Student name : " + name);
    };
    return CourseBatch;
}());
exports.CourseBatch = CourseBatch;
