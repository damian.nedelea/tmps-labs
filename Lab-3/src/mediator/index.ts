namespace MediatorArea {
    // Company's Departments - Colleagues
    abstract class Department {
        constructor(public mediator: Mediator) {
        }
    }

    interface Handle {
    }

    interface Order extends Handle {
        handleOrder(): void;
    }

    interface Hiring extends Handle {
        handleHiring(): void;
    }

    interface Payment extends Handle {
        handlePayment(): void;
    }

    class MarketingDepartment extends Department implements Order {
        public handleOrder(): void {
            setup.addRecord('Marketing Department got the order.');
        }
    }

    class SalesDepartment extends Department implements Order {
        public handleOrder(): void {
            setup.addRecord('Sales Department got the order.');
        }
    }

    class SecurityDepartment extends Department implements Hiring {
        public handleHiring(): void {
            setup.addRecord('Security Department informed about new job application.');
        }
    }

    class FinanceDepartment extends Department implements Order, Payment {
        public handleOrder(): void {
            setup.addRecord('Finance Department got the order.');
        }

        public handlePayment(): void {
            setup.addRecord('Finance Department approved pay stubs.');
        }
    }

    class LogisticsDepartment extends Department implements Order {
        public handleOrder(): void {
            setup.addRecord('Logistics Department got the order.');
        }
    }

    class HRDepartment extends Department implements Hiring, Payment {
        public handleHiring(): void {
            setup.addRecord('HR Department informed about new job application.');
        }

        public handlePayment(): void {
            setup.addRecord('HR Department approved pay stubs.');
        }
    }

    class AdministrativeDepartment extends Department implements Hiring {
        public handleHiring(): void {
            setup.addRecord('Administrative Department informed about new job application.');
        }
    }

    // Mediator
    class Mediator {
        private events: Record<string, any> = {};

        public subscribe(eventKey: string, callback: Function): void {
            if (!this.events[eventKey]) this.events[eventKey] = [];
            this.events[eventKey].push(callback);
        }

        public publish = (event: string): void => {
            for (let prop in this.events) {
                if (!this.events.hasOwnProperty(prop)) continue;
                if (prop == event) this.events[prop].forEach((callback: Function) => callback());
            }
        }
    }

    // Run it all, that's Facade
    class Setup {
        private logCont: HTMLElement = document.getElementById('logs') as HTMLElement;

        public addRecord(msg: string): void { // add record
            let record: HTMLElement = document.createElement('div');
            record.textContent = msg;
            this.logCont.appendChild(record);
        }

        public run(): void {
            let mediator = new Mediator();
            let salesDpt = new SalesDepartment(mediator);
            let finDpt = new FinanceDepartment(mediator);
            let logistDpt = new LogisticsDepartment(mediator);
            let marketDpt = new MarketingDepartment(mediator);
            let securDpt = new SecurityDepartment(mediator);
            let hrDpt = new HRDepartment(mediator);
            let adminDpt = new AdministrativeDepartment(mediator);
            // Departments involved in sales subscribe for orderPlaced event
            salesDpt.mediator.subscribe('orderPlaced', salesDpt.handleOrder);
            marketDpt.mediator.subscribe('orderPlaced', marketDpt.handleOrder);
            finDpt.mediator.subscribe('orderPlaced', finDpt.handleOrder);
            logistDpt.mediator.subscribe('orderPlaced', logistDpt.handleOrder);
            // Departments involved in hiring subscribe for personApplied event
            securDpt.mediator.subscribe('personApplied', securDpt.handleHiring);
            hrDpt.mediator.subscribe('personApplied', hrDpt.handleHiring);
            adminDpt.mediator.subscribe('personApplied', adminDpt.handleHiring);
            // Departments involved in payments subscribe for salaryPaid event
            finDpt.mediator.subscribe('salaryPaid', finDpt.handlePayment);
            hrDpt.mediator.subscribe('salaryPaid', hrDpt.handlePayment);

            let btn1: HTMLElement = document.getElementById("sender1") as HTMLElement;
            btn1.addEventListener("click", () => mediator.publish('orderPlaced'));
            let btn2: HTMLElement = document.getElementById("sender2") as HTMLElement;
            btn2.addEventListener("click", () => mediator.publish('personApplied'));
            let btn3: HTMLElement = document.getElementById("sender3") as HTMLElement;
            btn3.addEventListener("click", () => mediator.publish('salaryPaid'));

            let reset: HTMLElement = document.getElementById("reset") as HTMLElement;
            reset.addEventListener("click", () => this.removeMsg(this.logCont));
        }

        private removeMsg(elem: HTMLElement) {
            let msgs: HTMLElement[] = Array.prototype.slice.call(elem.children);
            msgs.forEach((element: HTMLElement) => {
                if (element.tagName != 'header'.toUpperCase()) element.remove();
            });
        }
    }

    // Client. No idea what is going on inside our Facade
    let setup = new Setup();
    setup.run();
}
