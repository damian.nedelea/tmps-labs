import axios from "axios";
import {ConstantPolicy} from "./constant-policy";
import {Retryer} from "./retryer";

async function callAPI(
    endpoint: string,
    payload: any,
): Promise<any> {
    try {
        const result = await axios.post(
            endpoint,
            {
                ...payload,
            },
        );
        if (result && result.data) {
            return result.data;
        }
    } catch (err) {
        console.log(err);
    }
}

export default async () => {
    const policy = new ConstantPolicy()

    const endpoint = 'v1/mickey/mouse'

    const payload = {id: 1, ears: 'big', braces: true}

    const apiCommand = new ApiCommand(callAPI, endpoint, payload)

    let result;
    try {
        result = await Retryer(apiCommand, policy)
    } catch(err) {
        console.log(err);
    }
}

