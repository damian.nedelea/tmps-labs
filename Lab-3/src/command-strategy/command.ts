interface ICommand {
    execute(): any;
}

class ApiCommand<T, U> implements ICommand {
    constructor(private fn: (endpoint: string, payload: T) => Promise<U>, private endpoint: string, private payload: T) {}

    public async execute(): Promise<U> {
        return await this.fn(this.endpoint, this.payload);
    }
}
