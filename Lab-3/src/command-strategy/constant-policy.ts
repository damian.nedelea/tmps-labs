export interface IPolicy {
    maxTries: number;
    currentWait: () => number;
    shouldRetry: (err: any) => boolean;
    incrementTry: () => void;
}

export class ConstantPolicy implements IPolicy {
    private retryCount: number;
    constructor(public maxTries: number = 5, private waitTime: number = 500) {
        this.retryCount = 0;
    }
    currentWait() {
        return this.waitTime;
    }
    shouldRetry(err: any): boolean {
        if (err.response && err.response.status >= 400) {
            return false
        } else if (this.retryCount < this.maxTries) {
            return true;
        }
        return false;
    }
    incrementTry() {
        this.retryCount++;
    }
}
