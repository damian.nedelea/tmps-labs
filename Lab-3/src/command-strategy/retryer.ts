import {IPolicy} from "./constant-policy";

export async function Retryer(command: ICommand, policy: IPolicy) {
    while (true) {
        try {
            policy.incrementTry();
            return await command.execute();
        } catch (error) {
            if (policy.shouldRetry(error)) {
                await new Promise(resolve => setTimeout(resolve, policy.currentWait()));
            } else {
                return; // Tell the humans!
            }
        }
    }
}
