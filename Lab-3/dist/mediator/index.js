"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var MediatorArea;
(function (MediatorArea) {
    // Company's Departments - Colleagues
    var Department = /** @class */ (function () {
        function Department(mediator) {
            this.mediator = mediator;
        }
        return Department;
    }());
    var MarketingDepartment = /** @class */ (function (_super) {
        __extends(MarketingDepartment, _super);
        function MarketingDepartment() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        MarketingDepartment.prototype.handleOrder = function () {
            setup.addRecord('Marketing Department got the order.');
        };
        return MarketingDepartment;
    }(Department));
    var SalesDepartment = /** @class */ (function (_super) {
        __extends(SalesDepartment, _super);
        function SalesDepartment() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        SalesDepartment.prototype.handleOrder = function () {
            setup.addRecord('Sales Department got the order.');
        };
        return SalesDepartment;
    }(Department));
    var SecurityDepartment = /** @class */ (function (_super) {
        __extends(SecurityDepartment, _super);
        function SecurityDepartment() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        SecurityDepartment.prototype.handleHiring = function () {
            setup.addRecord('Security Department informed about new job application.');
        };
        return SecurityDepartment;
    }(Department));
    var FinanceDepartment = /** @class */ (function (_super) {
        __extends(FinanceDepartment, _super);
        function FinanceDepartment() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        FinanceDepartment.prototype.handleOrder = function () {
            setup.addRecord('Finance Department got the order.');
        };
        FinanceDepartment.prototype.handlePayment = function () {
            setup.addRecord('Finance Department approved pay stubs.');
        };
        return FinanceDepartment;
    }(Department));
    var LogisticsDepartment = /** @class */ (function (_super) {
        __extends(LogisticsDepartment, _super);
        function LogisticsDepartment() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        LogisticsDepartment.prototype.handleOrder = function () {
            setup.addRecord('Logistics Department got the order.');
        };
        return LogisticsDepartment;
    }(Department));
    var HRDepartment = /** @class */ (function (_super) {
        __extends(HRDepartment, _super);
        function HRDepartment() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        HRDepartment.prototype.handleHiring = function () {
            setup.addRecord('HR Department informed about new job application.');
        };
        HRDepartment.prototype.handlePayment = function () {
            setup.addRecord('HR Department approved pay stubs.');
        };
        return HRDepartment;
    }(Department));
    var AdministrativeDepartment = /** @class */ (function (_super) {
        __extends(AdministrativeDepartment, _super);
        function AdministrativeDepartment() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        AdministrativeDepartment.prototype.handleHiring = function () {
            setup.addRecord('Administrative Department informed about new job application.');
        };
        return AdministrativeDepartment;
    }(Department));
    // Mediator
    var Mediator = /** @class */ (function () {
        function Mediator() {
            var _this = this;
            this.events = {};
            this.publish = function (event) {
                for (var prop in _this.events) {
                    if (!_this.events.hasOwnProperty(prop))
                        continue;
                    if (prop == event)
                        _this.events[prop].forEach(function (callback) { return callback(); });
                }
            };
        }
        Mediator.prototype.subscribe = function (eventKey, callback) {
            if (!this.events[eventKey])
                this.events[eventKey] = [];
            this.events[eventKey].push(callback);
        };
        return Mediator;
    }());
    // Run it all, that's Facade
    var Setup = /** @class */ (function () {
        function Setup() {
            this.logCont = document.getElementById('logs');
        }
        Setup.prototype.addRecord = function (msg) {
            var record = document.createElement('div');
            record.textContent = msg;
            this.logCont.appendChild(record);
        };
        Setup.prototype.removeMsg = function (elem) {
            var msgs = Array.prototype.slice.call(elem.children);
            msgs.forEach(function (element) {
                if (element.tagName != 'header'.toUpperCase())
                    element.remove();
            });
        };
        Setup.prototype.run = function () {
            var _this = this;
            var mediator = new Mediator();
            var salesDpt = new SalesDepartment(mediator);
            var finDpt = new FinanceDepartment(mediator);
            var logistDpt = new LogisticsDepartment(mediator);
            var marketDpt = new MarketingDepartment(mediator);
            var securDpt = new SecurityDepartment(mediator);
            var hrDpt = new HRDepartment(mediator);
            var adminDpt = new AdministrativeDepartment(mediator);
            // Departments involved in sales subscribe for orderPlaced event
            salesDpt.mediator.subscribe('orderPlaced', salesDpt.handleOrder);
            marketDpt.mediator.subscribe('orderPlaced', marketDpt.handleOrder);
            finDpt.mediator.subscribe('orderPlaced', finDpt.handleOrder);
            logistDpt.mediator.subscribe('orderPlaced', logistDpt.handleOrder);
            // Departments involved in hiring subscribe for personApplied event
            securDpt.mediator.subscribe('personApplied', securDpt.handleHiring);
            hrDpt.mediator.subscribe('personApplied', hrDpt.handleHiring);
            adminDpt.mediator.subscribe('personApplied', adminDpt.handleHiring);
            // Departments involved in payments subscribe for salaryPaid event
            finDpt.mediator.subscribe('salaryPaid', finDpt.handlePayment);
            hrDpt.mediator.subscribe('salaryPaid', hrDpt.handlePayment);
            var btn1 = document.getElementById("sender1");
            btn1.addEventListener("click", function () { return mediator.publish('orderPlaced'); });
            var btn2 = document.getElementById("sender2");
            btn2.addEventListener("click", function () { return mediator.publish('personApplied'); });
            var btn3 = document.getElementById("sender3");
            btn3.addEventListener("click", function () { return mediator.publish('salaryPaid'); });
            var reset = document.getElementById("reset");
            reset.addEventListener("click", function () { return _this.removeMsg(_this.logCont); });
        };
        return Setup;
    }());
    // Client. No idea what is going on inside our Facade
    var setup = new Setup();
    setup.run();
})(MediatorArea || (MediatorArea = {}));
