"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConstantPolicy = void 0;
var ConstantPolicy = /** @class */ (function () {
    function ConstantPolicy(maxTries, waitTime) {
        if (maxTries === void 0) { maxTries = 5; }
        if (waitTime === void 0) { waitTime = 500; }
        this.maxTries = maxTries;
        this.waitTime = waitTime;
        this.retryCount = 0;
    }
    ConstantPolicy.prototype.currentWait = function () {
        return this.waitTime;
    };
    ConstantPolicy.prototype.shouldRetry = function (err) {
        if (err.response && err.response.status >= 400) {
            return false;
        }
        else if (this.retryCount < this.maxTries) {
            return true;
        }
        return false;
    };
    ConstantPolicy.prototype.incrementTry = function () {
        this.retryCount++;
    };
    return ConstantPolicy;
}());
exports.ConstantPolicy = ConstantPolicy;
