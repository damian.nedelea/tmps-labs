"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExpoPolicy = void 0;
var ExpoPolicy = /** @class */ (function () {
    function ExpoPolicy(maxTries, initWaitTime) {
        if (maxTries === void 0) { maxTries = 5; }
        if (initWaitTime === void 0) { initWaitTime = 500; }
        this.maxTries = maxTries;
        this.initWaitTime = initWaitTime;
        this.retryCount = 0;
    }
    ExpoPolicy.prototype.currentWait = function () {
        return Math.pow(this.initWaitTime, this.retryCount);
    };
    ExpoPolicy.prototype.shouldRetry = function (err) {
        if (err.response && err.response.status >= 400) {
            return false;
        }
        else if (this.retryCount < this.maxTries) {
            return true;
        }
        return false;
    };
    ExpoPolicy.prototype.incrementTry = function () {
        this.retryCount++;
    };
    return ExpoPolicy;
}());
exports.ExpoPolicy = ExpoPolicy;
