"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var adapter_1 = require("./adapter");
var gAdapter = new adapter_1.GeocodingAdapter(51.5074, 0.1278, 'Locator');
var gAdapter2 = new adapter_1.GeocodingAdapter(51.5074, 0.1278, 'GeoCoder');
var locationFromLocator = gAdapter.locate();
var locationFromGeoCoder = gAdapter2.locate();
console.log(locationFromLocator);
console.log(locationFromGeoCoder);
