"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Locator = void 0;
var Locator = /** @class */ (function () {
    function Locator(latitude, longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
    Locator.prototype.geocode = function () {
        return "The specified location (" + this.latitude + ", " + this.longitude + ") returns London.";
    };
    return Locator;
}());
exports.Locator = Locator;
