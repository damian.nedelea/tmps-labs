"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GeoCoder = void 0;
var GeoCoder = /** @class */ (function () {
    function GeoCoder(latitude, longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
    GeoCoder.prototype.locate = function () {
        return this.latitude + " | " + this.longitude + " is in London.";
    };
    return GeoCoder;
}());
exports.GeoCoder = GeoCoder;
