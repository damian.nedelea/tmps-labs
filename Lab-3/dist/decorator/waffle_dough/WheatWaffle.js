"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.WheatWaffle = void 0;
var BaseWaffle_1 = require("./BaseWaffle");
var WheatWaffle = /** @class */ (function (_super) {
    __extends(WheatWaffle, _super);
    function WheatWaffle() {
        var _this = _super.call(this) || this;
        _this.description = "waffle dough with wheat flour";
        _this.price = 15.0;
        return _this;
    }
    return WheatWaffle;
}(BaseWaffle_1.BaseWaffle));
exports.WheatWaffle = WheatWaffle;
