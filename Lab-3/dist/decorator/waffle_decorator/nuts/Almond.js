"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.Almond = void 0;
var BaseNuts_1 = require("./BaseNuts");
var Almond = /** @class */ (function (_super) {
    __extends(Almond, _super);
    function Almond(waffleToDecorate) {
        var _this = _super.call(this, waffleToDecorate) || this;
        _this.price = 1.00;
        return _this;
    }
    Almond.prototype.getDescription = function () {
        return this.baseWaffle.getDescription() + " , Almond  ";
    };
    return Almond;
}(BaseNuts_1.BaseNuts));
exports.Almond = Almond;
