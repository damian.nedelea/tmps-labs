"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseFruit = void 0;
var BaseDecorator_1 = require("../BaseDecorator");
var BaseFruit = /** @class */ (function (_super) {
    __extends(BaseFruit, _super);
    function BaseFruit(waffleToDecorate) {
        var _this = _super.call(this) || this;
        _this.baseWaffle = waffleToDecorate;
        return _this;
    }
    BaseFruit.prototype.getPrice = function () {
        return (this.baseWaffle.getPrice() + this.price);
    };
    return BaseFruit;
}(BaseDecorator_1.BaseDecorator));
exports.BaseFruit = BaseFruit;
