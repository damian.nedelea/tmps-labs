"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.Kiwi = void 0;
var BaseFruit_1 = require("./BaseFruit");
var Kiwi = /** @class */ (function (_super) {
    __extends(Kiwi, _super);
    function Kiwi(waffleToDecorate) {
        var _this = _super.call(this, waffleToDecorate) || this;
        _this.price = 0.5;
        return _this;
    }
    Kiwi.prototype.getDescription = function () {
        return this.baseWaffle.getDescription() + " , Kiwi ";
    };
    return Kiwi;
}(BaseFruit_1.BaseFruit));
exports.Kiwi = Kiwi;
