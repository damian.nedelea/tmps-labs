"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseDecorator = void 0;
var BaseWaffle_1 = require("../waffle_dough/BaseWaffle");
var BaseDecorator = /** @class */ (function (_super) {
    __extends(BaseDecorator, _super);
    function BaseDecorator() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    BaseDecorator.prototype.getDescription = function () {
        return _super.prototype.getDescription.call(this);
    };
    return BaseDecorator;
}(BaseWaffle_1.BaseWaffle));
exports.BaseDecorator = BaseDecorator;
