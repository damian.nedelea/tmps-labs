"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.WhiteChocolate = void 0;
var BaseChocolate_1 = require("./BaseChocolate");
var WhiteChocolate = /** @class */ (function (_super) {
    __extends(WhiteChocolate, _super);
    function WhiteChocolate(waffleToDecorate) {
        var _this = _super.call(this, waffleToDecorate) || this;
        _this.price = 2.0;
        return _this;
    }
    WhiteChocolate.prototype.getDescription = function () {
        return this.baseWaffle.getDescription() + " , White Chocolate ";
    };
    return WhiteChocolate;
}(BaseChocolate_1.BaseChocolate));
exports.WhiteChocolate = WhiteChocolate;
