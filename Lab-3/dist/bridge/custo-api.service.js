"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomApiService = void 0;
var CustomApiService = /** @class */ (function () {
    function CustomApiService() {
    }
    CustomApiService.prototype.get = function () {
        return {};
    };
    CustomApiService.prototype.getAll = function () {
        return [];
    };
    CustomApiService.prototype.add = function (entity) {
        // etc
    };
    CustomApiService.prototype.update = function (entity) {
        // etc
    };
    CustomApiService.prototype.delete = function (entity) {
        // etc
    };
    return CustomApiService;
}());
exports.CustomApiService = CustomApiService;
