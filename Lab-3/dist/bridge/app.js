"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MongoDBService = void 0;
var user_service_1 = require("./user.service");
var MongoDBService = /** @class */ (function () {
    function MongoDBService() {
    }
    MongoDBService.prototype.add = function (entity) {
    };
    MongoDBService.prototype.delete = function (entity) {
    };
    MongoDBService.prototype.get = function () {
        return {};
    };
    MongoDBService.prototype.getAll = function () {
        return [];
    };
    MongoDBService.prototype.update = function (entity) {
    };
    return MongoDBService;
}());
exports.MongoDBService = MongoDBService;
// Swap out the api service
var apiService = new MongoDBService();
// No need to change the rest!
var userService = new user_service_1.UserService(apiService);
userService.addNewUser({});
