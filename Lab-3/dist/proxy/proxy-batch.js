"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProxyBatch = void 0;
/**
 * The proxy class controlling access to the CourseBatch.
 *
 */
var ProxyBatch = /** @class */ (function () {
    function ProxyBatch(batch) {
        this.TOTAL_STUDENTS_ALLOWED_TO_BATCH = 10;
        this.batch = batch;
    }
    ProxyBatch.prototype.totalStudents = function () {
        return this.batch.totalStudents();
    };
    ProxyBatch.prototype.registerStudent = function (name) {
        if (this.TOTAL_STUDENTS_ALLOWED_TO_BATCH >= this.totalStudents()) {
            this.batch.registerStudent(name);
        }
        else {
            console.log("Course batch size is : " + this.TOTAL_STUDENTS_ALLOWED_TO_BATCH);
            console.log("Batch is full so further students are not allowed ");
        }
    };
    return ProxyBatch;
}());
exports.ProxyBatch = ProxyBatch;
