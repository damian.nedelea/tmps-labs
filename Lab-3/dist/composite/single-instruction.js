"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SingleInstruction = void 0;
var SingleInstruction = /** @class */ (function () {
    function SingleInstruction(name) {
        this.name = name;
    }
    return SingleInstruction;
}());
exports.SingleInstruction = SingleInstruction;
