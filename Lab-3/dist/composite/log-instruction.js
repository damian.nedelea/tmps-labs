"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.LogInstruction = void 0;
var single_instruction_1 = require("./single-instruction");
var LogInstruction = /** @class */ (function (_super) {
    __extends(LogInstruction, _super);
    function LogInstruction(name, log) {
        var _this = _super.call(this, name) || this;
        _this.log = log;
        return _this;
    }
    LogInstruction.prototype.execute = function () {
        console.log(this.name + ": " + this.log);
        return true;
    };
    return LogInstruction;
}(single_instruction_1.SingleInstruction));
exports.LogInstruction = LogInstruction;
