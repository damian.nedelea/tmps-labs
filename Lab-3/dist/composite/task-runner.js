"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TaskRunner = void 0;
var TaskRunner = /** @class */ (function () {
    function TaskRunner(tasks) {
        this.tasks = tasks;
    }
    TaskRunner.prototype.runTasks = function () {
        for (var _i = 0, _a = this.tasks; _i < _a.length; _i++) {
            var task = _a[_i];
            task.execute();
        }
    };
    return TaskRunner;
}());
exports.TaskRunner = TaskRunner;
