import {CarFactory} from "./car-factory";
import {CarLocation, CarType} from "./car";

console.log(CarFactory.buildCar(CarType.BUS, CarLocation.GERMAN));
console.log(CarFactory.buildCar(CarType.MINI, CarLocation.INDIA));
console.log(CarFactory.buildCar(CarType.LUXURY, CarLocation.USA));
