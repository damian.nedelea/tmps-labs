import {Car, CarLocation, CarType} from "./car";

export class MiniCar extends Car {
    constructor(location: CarLocation) {
        super(CarType.MINI, location);
        this.construct();
    }

    protected construct(): void {
        console.log("Connecting to mini car");
    }
}
