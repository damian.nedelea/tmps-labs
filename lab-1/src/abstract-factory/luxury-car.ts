import {Car, CarLocation, CarType} from "./car";


export class LuxuryCar extends Car {
    constructor(location: CarLocation) {
        super(CarType.LUXURY, location);
        this.construct();
    }

    protected construct(): void {
        console.log("Connecting to luxury car");
    }
}
