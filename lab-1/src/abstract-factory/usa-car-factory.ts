import {Car, CarLocation, CarType} from "./car";
import {MiniCar} from "./mini-car";
import {LuxuryCar} from "./luxury-car";
import {BusCar} from "./bus-car";

export class UsaCarFactory {
    static buildCar(model: CarType): Car {
        let car: Car;
        switch (model) {
            case CarType.BUS:
                car = new BusCar(CarLocation.USA);
                break;

            case CarType.MINI:
                car = new MiniCar(CarLocation.USA);
                break;

            case CarType.LUXURY:
                car = new LuxuryCar(CarLocation.USA);
                break;

            default:
                break;

        }
        // @ts-ignore
        return car;
    }
}
