import {Car, CarLocation, CarType} from "./car";
import {MiniCar} from "./mini-car";
import {LuxuryCar} from "./luxury-car";
import {BusCar} from "./bus-car";

export class IndiaCarFactory {
    static buildCar(model: CarType): Car {
        let car: Car;
        switch (model) {
            case CarType.BUS:
                car = new BusCar(CarLocation.INDIA);
                break;

            case CarType.MINI:
                car = new MiniCar(CarLocation.INDIA);
                break;

            case CarType.LUXURY:
                car = new LuxuryCar(CarLocation.INDIA);
                break;

            default:
                break;

        }
        // @ts-ignore
        return car;
    }
}
