import {Car, CarLocation, CarType} from "./car";

export class BusCar extends Car {
    constructor(location: CarLocation) {
        super(CarType.BUS, location);
        this.construct();
    }

    protected construct(): void {
        console.log("Connecting to bus car");
    }
}
