import {Car, CarLocation, CarType} from "./car";
import {IndiaCarFactory} from "./india-car-factory";
import {UsaCarFactory} from "./usa-car-factory";
import {GermanCarFactory} from "./german-car-factory";

export class CarFactory {
    public static buildCar(type: CarType, location: CarLocation): Car {
        let car: Car;

        switch (location) {
            case CarLocation.USA:
                car = UsaCarFactory.buildCar(type);
                break;

            case CarLocation.INDIA:
                car = IndiaCarFactory.buildCar(type);
                break;

            case CarLocation.GERMAN:
                car = GermanCarFactory.buildCar(type);
                break;

            default:
                car = GermanCarFactory.buildCar(type);

        }

        return car;
    }
}
