export enum CarType {
    BUS = "BUS", MINI = "MINI", LUXURY = "LUXURY"
}

export enum CarLocation {
    GERMAN = "GERMAN", USA = "USA", INDIA ="INDIA"
}


export abstract class Car {

    public model: CarType;
    public location: CarLocation;

    constructor(model: CarType, location: CarLocation) {
        this.model = model;
        this.location = location;
    }

    protected abstract construct(): void;

    public getModel(): CarType {
        return this.model;
    }

    setModel(model: CarType): void {
        this.model = model;
    }

    getLocation(): CarLocation {
        return this.location;
    }

    setLocation(location: CarLocation): void {
        this.location = location;
    }

    public toString(): string {
        return "CarModel - " + this.model + " located in " + this.location;
    }
}
