import {Car, CarLocation, CarType} from "./car";
import {BusCar} from "./bus-car";
import {MiniCar} from "./mini-car";
import {LuxuryCar} from "./luxury-car";

export class GermanCarFactory {
    static buildCar(model: CarType): Car {
        let car: Car;
        switch (model) {
            case CarType.BUS:
                car = new BusCar(CarLocation.GERMAN);
                break;

            case CarType.MINI:
                car = new MiniCar(CarLocation.GERMAN);
                break;

            case CarType.LUXURY:
                car = new LuxuryCar(CarLocation.GERMAN);
                break;

            default:
                break;

        }
        // @ts-ignore
        return car;
    }
}
