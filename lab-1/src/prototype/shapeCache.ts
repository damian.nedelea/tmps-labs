import {Shape} from "./shape";
import {Circle} from "./circle";
import {Square} from "./square";
import {Rectangle} from "./rectangle";

interface HashTable<V> {
    [key: string]: V;
}

export class ShapeCache {

    private static shapeMap: HashTable<Shape> = {};

    public static getShape(shapeId: string): Shape {
        const cachedShape: Shape = ShapeCache.shapeMap[shapeId];
        return cachedShape.clone();
    }

    public static loadCache(): void {
        const circle: Circle = new Circle();
        circle.setId("1");
        ShapeCache.shapeMap[circle.getId()] = circle;

        const square: Square = new Square();
        square.setId("2");
        ShapeCache.shapeMap[square.getId()] = square;

        const rectangle: Rectangle = new Rectangle();
        rectangle.setId("3");
        ShapeCache.shapeMap[rectangle.getId()] = rectangle;
    }
}
