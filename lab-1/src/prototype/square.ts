import {Shape} from "./shape";

export class Square extends Shape {

    constructor() {
        super("Square")
    }

    public draw(): void {
        console.log("Inside Square::draw() method.");
    }
}
