import {Shape} from "./shape";

export class Rectangle extends Shape {

    constructor() {
        super("Rectangle");
    }

    public draw(): void {
        console.log("Inside Rectangle::draw() method.");
    }
}
