import {Shape} from "./shape";

export class Circle extends Shape {

    constructor() {
        super("Circle")
    }

    public draw(): void {
        console.log("Inside Circle::draw() method.");
    }
}
