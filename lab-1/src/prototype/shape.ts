import {Cloneable} from "./cloneable";

export abstract class Shape implements Cloneable {
    protected type: string;
    private id: string | undefined;

    constructor(type: string) {
        this.type = type;
    }

    abstract draw(): void;

    public getType(): string {
        return this.type;
    }

    public getId(): string {
        return this.id as string;
    }

    public setId(id: string): void {
        this.id = id;
    }

    public clone(): Shape {
        let clone = null;

        try {
            clone = Object.assign(Object.create(Object.getPrototypeOf(this)), this)

        } catch (e) {
            console.log(e);
        }

        return clone;
    }
}
