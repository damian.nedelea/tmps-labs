import {HouseBuilder} from "./builder";

const house = new HouseBuilder('John Street 14')
    .setFloor(4)
    .makeParking()
    .makeGarden()
    .build();

console.log(house);
