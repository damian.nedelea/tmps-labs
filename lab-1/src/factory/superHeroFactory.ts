import {Hero} from './hero';
import {Villain} from './villain';

export class SuperHeroFactory {

    createSuperHero(type: Object): any;
    createSuperHero(type: 'hero'): Hero;
    createSuperHero(type: 'villain'): Villain;

    public createSuperHero(heroOptions: any): Hero | Villain {
        if (heroOptions.type === "hero") {
            return new Hero(heroOptions.name, heroOptions.health);
        } else if (heroOptions.type === "villain") {
            return new Villain(heroOptions.name, heroOptions.health);
        } else {
            throw new Error('Select either a Hero or a Villain');
        }
    }
}
