const cluster = require('cluster');
const numCPUs = require('os').cpus().length;
const axios = require('axios');

const link = "https://xiaomistore.md/ro/smartphones/?filters%5Bmodel%5D%5B109%5D=109&filters%5Bproperty%5D%5B128%5D=4160%2C5368"

if (cluster.isMaster) {
    console.log(`Master ${process.pid} is running`);
    for (let i = 0; i < numCPUs; i++) {
        cluster.fork();
    }
    for (let i = 0; i < 100; i++) {
        axios.get(link).then(res => {
            console.log(res);
        }, (err) => {
            console.log(err);
        })
    }

    cluster.on('exit', (worker, code, signal) => {
        console.log(`worker ${worker.process.pid} died`);
    });
} else {

    for (let i = 0; i < 100; i++) {
        axios.get(link).then(res => {
            console.log(res);
        }, (err) => {
            console.log(err);
        })
    }

    console.log(`Worker ${process.pid} started`);
}
