"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.House = void 0;
var House = /** @class */ (function () {
    function House(houseBuilder) {
        this.address = houseBuilder.getAddress();
        this.floorNumber = houseBuilder.getFloorNumber();
        this.isHavingParking = houseBuilder.isHavingParking();
        this.isHavingGarden = houseBuilder.isHavingGarden();
    }
    return House;
}());
exports.House = House;
