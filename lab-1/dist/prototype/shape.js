"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Shape = void 0;
var Shape = /** @class */ (function () {
    function Shape(type) {
        this.type = type;
    }
    Shape.prototype.getType = function () {
        return this.type;
    };
    Shape.prototype.getId = function () {
        return this.id;
    };
    Shape.prototype.setId = function (id) {
        this.id = id;
    };
    Shape.prototype.clone = function () {
        var clone = null;
        try {
            clone = Object.assign(Object.create(Object.getPrototypeOf(this)), this);
        }
        catch (e) {
            console.log(e);
        }
        return clone;
    };
    return Shape;
}());
exports.Shape = Shape;
