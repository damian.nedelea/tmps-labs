"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShapeCache = void 0;
var circle_1 = require("./circle");
var square_1 = require("./square");
var rectangle_1 = require("./rectangle");
var ShapeCache = /** @class */ (function () {
    function ShapeCache() {
    }
    ShapeCache.getShape = function (shapeId) {
        var cachedShape = ShapeCache.shapeMap[shapeId];
        return cachedShape.clone();
    };
    ShapeCache.loadCache = function () {
        var circle = new circle_1.Circle();
        circle.setId("1");
        ShapeCache.shapeMap[circle.getId()] = circle;
        var square = new square_1.Square();
        square.setId("2");
        ShapeCache.shapeMap[square.getId()] = square;
        var rectangle = new rectangle_1.Rectangle();
        rectangle.setId("3");
        ShapeCache.shapeMap[rectangle.getId()] = rectangle;
    };
    ShapeCache.shapeMap = {};
    return ShapeCache;
}());
exports.ShapeCache = ShapeCache;
