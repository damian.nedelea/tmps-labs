"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PrototypePatternDemo = void 0;
var shapeCache_1 = require("./shapeCache");
var PrototypePatternDemo = /** @class */ (function () {
    function PrototypePatternDemo() {
        shapeCache_1.ShapeCache.loadCache();
        var clonedShape = shapeCache_1.ShapeCache.getShape("1");
        console.log("Shape : " + clonedShape.getType());
        var clonedShape2 = shapeCache_1.ShapeCache.getShape("2");
        console.log("Shape : " + clonedShape2.getType());
        var clonedShape3 = shapeCache_1.ShapeCache.getShape("3");
        console.log("Shape : " + clonedShape3.getType());
    }
    return PrototypePatternDemo;
}());
exports.PrototypePatternDemo = PrototypePatternDemo;
new PrototypePatternDemo();
