"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var builder_1 = require("./builder");
var house = new builder_1.HouseBuilder('John Street 14')
    .setFloor(4)
    .makeParking()
    .makeGarden()
    .build();
console.log(house);
