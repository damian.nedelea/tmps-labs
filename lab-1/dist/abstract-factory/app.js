"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var car_factory_1 = require("./car-factory");
var car_1 = require("./car");
console.log(car_factory_1.CarFactory.buildCar(car_1.CarType.BUS, car_1.CarLocation.GERMAN));
console.log(car_factory_1.CarFactory.buildCar(car_1.CarType.MINI, car_1.CarLocation.INDIA));
console.log(car_factory_1.CarFactory.buildCar(car_1.CarType.LUXURY, car_1.CarLocation.USA));
