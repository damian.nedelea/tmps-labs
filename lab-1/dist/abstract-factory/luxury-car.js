"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.LuxuryCar = void 0;
var car_1 = require("./car");
var LuxuryCar = /** @class */ (function (_super) {
    __extends(LuxuryCar, _super);
    function LuxuryCar(location) {
        var _this = _super.call(this, car_1.CarType.LUXURY, location) || this;
        _this.construct();
        return _this;
    }
    LuxuryCar.prototype.construct = function () {
        console.log("Connecting to luxury car");
    };
    return LuxuryCar;
}(car_1.Car));
exports.LuxuryCar = LuxuryCar;
