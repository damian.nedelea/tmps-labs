"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IndiaCarFactory = void 0;
var car_1 = require("./car");
var mini_car_1 = require("./mini-car");
var luxury_car_1 = require("./luxury-car");
var bus_car_1 = require("./bus-car");
var IndiaCarFactory = /** @class */ (function () {
    function IndiaCarFactory() {
    }
    IndiaCarFactory.buildCar = function (model) {
        var car;
        switch (model) {
            case car_1.CarType.BUS:
                car = new bus_car_1.BusCar(car_1.CarLocation.INDIA);
                break;
            case car_1.CarType.MINI:
                car = new mini_car_1.MiniCar(car_1.CarLocation.INDIA);
                break;
            case car_1.CarType.LUXURY:
                car = new luxury_car_1.LuxuryCar(car_1.CarLocation.INDIA);
                break;
            default:
                break;
        }
        // @ts-ignore
        return car;
    };
    return IndiaCarFactory;
}());
exports.IndiaCarFactory = IndiaCarFactory;
