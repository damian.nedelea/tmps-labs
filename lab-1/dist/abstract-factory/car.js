"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Car = exports.CarLocation = exports.CarType = void 0;
var CarType;
(function (CarType) {
    CarType["BUS"] = "BUS";
    CarType["MINI"] = "MINI";
    CarType["LUXURY"] = "LUXURY";
})(CarType = exports.CarType || (exports.CarType = {}));
var CarLocation;
(function (CarLocation) {
    CarLocation["GERMAN"] = "GERMAN";
    CarLocation["USA"] = "USA";
    CarLocation["INDIA"] = "INDIA";
})(CarLocation = exports.CarLocation || (exports.CarLocation = {}));
var Car = /** @class */ (function () {
    function Car(model, location) {
        this.model = model;
        this.location = location;
    }
    Car.prototype.getModel = function () {
        return this.model;
    };
    Car.prototype.setModel = function (model) {
        this.model = model;
    };
    Car.prototype.getLocation = function () {
        return this.location;
    };
    Car.prototype.setLocation = function (location) {
        this.location = location;
    };
    Car.prototype.toString = function () {
        return "CarModel - " + this.model + " located in " + this.location;
    };
    return Car;
}());
exports.Car = Car;
