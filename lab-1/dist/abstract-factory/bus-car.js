"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.BusCar = void 0;
var car_1 = require("./car");
var BusCar = /** @class */ (function (_super) {
    __extends(BusCar, _super);
    function BusCar(location) {
        var _this = _super.call(this, car_1.CarType.BUS, location) || this;
        _this.construct();
        return _this;
    }
    BusCar.prototype.construct = function () {
        console.log("Connecting to bus car");
    };
    return BusCar;
}(car_1.Car));
exports.BusCar = BusCar;
