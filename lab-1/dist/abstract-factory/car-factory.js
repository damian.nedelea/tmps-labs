"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CarFactory = void 0;
var car_1 = require("./car");
var india_car_factory_1 = require("./india-car-factory");
var usa_car_factory_1 = require("./usa-car-factory");
var german_car_factory_1 = require("./german-car-factory");
var CarFactory = /** @class */ (function () {
    function CarFactory() {
    }
    CarFactory.buildCar = function (type, location) {
        var car;
        switch (location) {
            case car_1.CarLocation.USA:
                car = usa_car_factory_1.UsaCarFactory.buildCar(type);
                break;
            case car_1.CarLocation.INDIA:
                car = india_car_factory_1.IndiaCarFactory.buildCar(type);
                break;
            case car_1.CarLocation.GERMAN:
                car = german_car_factory_1.GermanCarFactory.buildCar(type);
                break;
            default:
                car = german_car_factory_1.GermanCarFactory.buildCar(type);
        }
        return car;
    };
    return CarFactory;
}());
exports.CarFactory = CarFactory;
