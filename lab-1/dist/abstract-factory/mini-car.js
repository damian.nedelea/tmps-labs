"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.MiniCar = void 0;
var car_1 = require("./car");
var MiniCar = /** @class */ (function (_super) {
    __extends(MiniCar, _super);
    function MiniCar(location) {
        var _this = _super.call(this, car_1.CarType.MINI, location) || this;
        _this.construct();
        return _this;
    }
    MiniCar.prototype.construct = function () {
        console.log("Connecting to mini car");
    };
    return MiniCar;
}(car_1.Car));
exports.MiniCar = MiniCar;
