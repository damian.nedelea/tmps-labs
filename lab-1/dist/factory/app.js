"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var heroFactory = __importStar(require("./heroFactory"));
var superHeroFactory_1 = require("./superHeroFactory");
var spiderman = heroFactory.createHero('Peter', 100);
spiderman.introduce(); // Hello, I am Peter. Pleasure to meet you.
var superman = heroFactory.createHero('Clark', 100);
superman.introduce(); // Hello, I am Clark. Pleasure to meet you.
var superheroFactory = new superHeroFactory_1.SuperHeroFactory();
var batman = superheroFactory.createSuperHero({ name: 'Batman', type: "hero" });
var joker = superheroFactory.createSuperHero({ name: 'Joker', health: 50, type: "villain" });
batman.attacked(40); // Hero attacked: 40 -- 60
joker.attacked(40); // Villain attacked: 40 -- 10
joker.rampage(); // Joker restored health to 90
