"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createHero = exports.HeroFactory = void 0;
var HeroFactory = /** @class */ (function () {
    function HeroFactory(name, health) {
        this.name = name;
        this.health = health;
    }
    HeroFactory.prototype.introduce = function () {
        console.log("Hello, I am " + this.name + ". Pleasure to meet you.");
    };
    return HeroFactory;
}());
exports.HeroFactory = HeroFactory;
function createHero(name, health) {
    return new HeroFactory(name, health);
}
exports.createHero = createHero;
