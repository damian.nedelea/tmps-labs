"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Villain = void 0;
var Villain = /** @class */ (function () {
    function Villain(name, health) {
        if (health === void 0) { health = 100; }
        this.maxHealth = 100;
        this.name = name;
        if (health < this.maxHealth) {
            this.health = health;
        }
        else {
            this.health = this.maxHealth;
        }
    }
    Villain.prototype.rampage = function () {
        if (this.health <= 10) {
            this.health = this.maxHealth * 0.90;
            console.log(this.name + " restored health to " + this.health);
        }
        else {
            console.log(this.name + " is not weak enough");
        }
    };
    Villain.prototype.attacked = function (attackValue) {
        if (attackValue > this.health) {
            console.log(this.name + " is no more.");
        }
        else {
            this.health -= attackValue;
            console.log("Villain attacked: " + attackValue + " -- " + this.health);
        }
    };
    Villain.prototype.heal = function (healValue) {
        if (this.health + healValue > this.maxHealth) {
            console.log(this.name + " has max health of " + this.maxHealth);
        }
        else {
            this.health += healValue;
            console.log(this.name + " healed to " + this.health);
        }
    };
    return Villain;
}());
exports.Villain = Villain;
