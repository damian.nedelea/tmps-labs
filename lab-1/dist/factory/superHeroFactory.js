"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SuperHeroFactory = void 0;
var hero_1 = require("./hero");
var villain_1 = require("./villain");
var SuperHeroFactory = /** @class */ (function () {
    function SuperHeroFactory() {
    }
    SuperHeroFactory.prototype.createSuperHero = function (heroOptions) {
        if (heroOptions.type === "hero") {
            return new hero_1.Hero(heroOptions.name, heroOptions.health);
        }
        else if (heroOptions.type === "villain") {
            return new villain_1.Villain(heroOptions.name, heroOptions.health);
        }
        else {
            throw new Error('Select either a Hero or a Villain');
        }
    };
    return SuperHeroFactory;
}());
exports.SuperHeroFactory = SuperHeroFactory;
