"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Hero = void 0;
var Hero = /** @class */ (function () {
    function Hero(name, health) {
        if (health === void 0) { health = 100; }
        this.maxHealth = 100;
        this.name = name;
        if (health < this.maxHealth) {
            this.health = health;
        }
        else {
            this.health = this.maxHealth;
        }
    }
    Hero.prototype.attacked = function (attackValue) {
        if (attackValue > this.health) {
            console.log(this.name + " is no more.");
        }
        else {
            this.health -= attackValue;
            console.log("Hero attacked: " + attackValue + " -- " + this.health);
        }
    };
    Hero.prototype.heal = function (healValue) {
        if (this.health + healValue > this.maxHealth) {
            console.log(this.name + " has max health of " + this.maxHealth);
        }
        else {
            this.health += healValue;
            console.log(this.name + " healed to " + this.health);
        }
    };
    return Hero;
}());
exports.Hero = Hero;
